# Auto Project

set up Download 'create.bat', 'after.bat' and 'gitignore' and place them in your projects directory

# create.bat

run in project directory: `create.bat <1> <2>`

Where:<br/>
1. <1>: is the clone address (if this is the only argument, this will be the project name, and will clone it from https://gitlab.com/{git config user.username}/{this arg})
2. <2> - optional: is folder name to create (This will be also the commit message in the dev branch)


This will:<br/>
1. clone the project
2. create initial commit with gitignore (same gitignore from drive but ignore also "after.bat")
3. create after.bat in the folder
4. create and move to dev branch (will not connect it to the remote yet)
5. cd into the new folder

# after.bat

run in the project directory: `after.bat <1>`

<1> optional - if exist the commit will be name that variable content. If not then the commit is name the name of the folder.

This will:<br/>
1. commit all the changes with message as the folder name
2. push changes to dev


