@ echo off
set GitProjectName=%2
setlocal EnableDelayedExpansion
if [%2] EQU [] (
        set GitProjectName=%1
        FOR /F %%I IN ('git config "user.username"') DO (
            set GitUsername=%%I
            )
        git clone https://gitlab.com/!GitUsername!/!GitProjectName!.git

    ) else (
        set GitProjectName=%2
        >NUL git clone %1 %2
    )


if %errorlevel% NEQ 0 ( Echo ERROR FAILED AT clone, make sure your ssh is set up correctly  & GOTO error_exit )

echo "repo cloned..."
>NUL copy "gitignore" "%GitProjectName%/.gitignore"
>NUL copy NUL "%GitProjectName%/after.bat"
if %errorlevel% NEQ 0 ( Echo ERROR FAILED AT copy & GOTO error_exit)

>NUL set "search=projectName"
>NUL set "replace=%GitProjectName%"

for /f "delims=" %%i in (after.bat) do (
         setlocal enabledelayedexpansion
         set line=%%i
         >>"%GitProjectName%/after.bat" echo(!line:%search%=%replace%!
         endlocal
     )
if %errorlevel% NEQ 0 ( Echo ERROR FAILED AT Editing after.bat  & GOTO error_exit )
echo ".gitignore and after.bat created"

>NUL cd %GitProjectName%
>NUL git add .gitignore
>NUL git commit -m "Init commit"
if %errorlevel% NEQ 0 ( Echo ERROR FAILED AT creating init commit & GOTO error_exit )
>NUL git push origin master
if %errorlevel% NEQ 0 ( Echo ERROR FAILED AT pushing to master & GOTO error_exit )
echo "initial commit created and was pushed to master"
>NUL git checkout -b dev
set /a errorlevel=0
echo "checked out dev branch"
goto exit

:error_exit
echo Error %errorlevel% happened
set /a errorlevel=0

:exit
endlocal
if [%2] EQU [] (
    cd %1
) else (
    cd %2
)
echo on
